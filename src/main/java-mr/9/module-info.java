module com.zarbosoft.pidgoon {
	requires com.zarbosoft.rendaw.common;
	requires com.google.common;
	requires pcollections;
	exports com.zarbosoft.pidgoon;
	exports com.zarbosoft.pidgoon.bytes;
	exports com.zarbosoft.pidgoon.bytes.nodes;
	exports com.zarbosoft.pidgoon.bytes.stores;
	exports com.zarbosoft.pidgoon.events;
	exports com.zarbosoft.pidgoon.events.nodes;
	exports com.zarbosoft.pidgoon.events.stores;
	exports com.zarbosoft.pidgoon.internal;
	exports com.zarbosoft.pidgoon.nodes;
}