package com.zarbosoft.pidgoon.bytes;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;

public class Position implements com.zarbosoft.pidgoon.Position {
  private final InputStream stream;
  private final int bufUsed;
  final byte[] buf;
  public int localOffset = 0;
  public long absolute = 0;
  public long line = 0;
  public long column = 0;

  public Position(final InputStream stream) {
    this.stream = stream;
    buf = new byte[10 * 1024];
    try {
      bufUsed = stream.read(buf, 0, buf.length);
    } catch (final IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  private Position(final Position last) {
    stream = last.stream;
    if (last.localOffset + 1 < last.bufUsed) {
      bufUsed = last.bufUsed;
      buf = last.buf;
      localOffset = last.localOffset + 1;
    } else {
      buf = new byte[10 * 1024];
      try {
        bufUsed = stream.read(buf, 0, buf.length);
      } catch (final IOException e) {
        throw new UncheckedIOException(e);
      }
      localOffset = 0;
    }
    absolute = last.absolute + 1;
    if (bufUsed == -1) {
      line = last.line;
      column = last.column + 1;
    } else {
      final byte b = buf[localOffset];
      if (b == (byte) '\n') {
        line = last.line + 1;
        column = 0;
      } else {
        line = last.line;
        column = last.column + 1;
      }
    }
  }

  @Override
  public Position advance() {
    if (bufUsed == -1) {
      return null;
    }
    return new Position(this);
  }

  @Override
  public boolean isEOF() {
    return bufUsed == -1;
  }

  public Clip getStoreData() {
    return new Clip(get());
  }

  public Byte get() {
    return buf[localOffset];
  }
}
