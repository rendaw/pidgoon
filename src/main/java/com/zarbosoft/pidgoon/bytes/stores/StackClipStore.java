package com.zarbosoft.pidgoon.bytes.stores;

import com.zarbosoft.pidgoon.Store;
import com.zarbosoft.pidgoon.bytes.Clip;
import com.zarbosoft.pidgoon.internal.BranchingStack;
import com.zarbosoft.rendaw.common.Pair;
import org.pcollections.PVector;

import java.util.Map;
import java.util.function.Consumer;

public class StackClipStore extends ClipStore {

  private BranchingStack<Object> stack;

  public StackClipStore(Map env) {
    super(env);
    stack = null;
  }

  public StackClipStore() {
    this(null);
  }

  protected StackClipStore(
      final Object color,
      final PVector<Object> cutStops,
      final Map env,
      final BranchingStack<Clip> data,
      final BranchingStack<Object> stack) {
    super(color, cutStops, env, data);
    this.stack = stack;
  }

  public static StackClipStore stackSingleElement(final Store store1) {
    StackClipStore store = (StackClipStore) store1;
    final Object value = store.stackTop();
    store = store.popStack();
    final int length = store.stackTop();
    store = store.popStack();
    return store.pushStack(value).pushStack(length + 1);
  }

  public StackClipStore popStack() {
    final StackClipStore out = split();
    out.stack = stack.pop();
    return out;
  }

  @Override
  public <Y> Y split() {
    return (Y) new StackClipStore(color, cutStops, env, data, stack);
  }

  @Override
  public boolean hasResult() {
    return (stack != null) && stack.isLast();
  }

  @Override
  public Object result() {
    return stackTop();
  }

  public <T> T stackTop() {
    return (T) stack.top();
  }

  // NOTE direct copy of StackStore methods
  //

  public StackClipStore pushStack(final Object o) {
    final StackClipStore out = split();
    if (stack == null) {
      out.stack = new BranchingStack<>(o);
    } else {
      out.stack = stack.push(o);
    }
    return out;
  }

  public static StackClipStore stackDoubleElement(final Store store1) {
    StackClipStore store = (StackClipStore) store1;
    final Object right = store.stackTop();
    store = store.popStack();
    final Object left = store.stackTop();
    store = store.popStack();
    final int length = store.stackTop();
    store = store.popStack();
    return store.pushStack(new Pair<>(left, right)).pushStack(length + 1);
  }

  public static StackClipStore stackDoubleElement(final Store store1, final String name) {
    StackClipStore store = (StackClipStore) store1;
    final Object value = store.stackTop();
    store = store.popStack();
    final int length = store.stackTop();
    store = store.popStack();
    return store.pushStack(new Pair<>(name, value)).pushStack(length + 1);
  }

  public static <L, R> StackClipStore stackPopDoubleList(
      StackClipStore s, final Pair.Consumer<L, R> callback) {
    final Integer count = s.stackTop();
    s = s.popStack();
    return stackPopDoubleList(s, count, callback);
  }

  public static <L, R> StackClipStore stackPopDoubleList(
      StackClipStore s, final int length, final Pair.Consumer<L, R> callback) {
    for (int i = 0; i < length; ++i) {
      final Object l = s.stackTop();
      s = s.popStack();
      final Object r = s.stackTop();
      s = s.popStack();
      callback.accept((L) l, (R) r);
    }
    return s;
  }

  public static <T> StackClipStore stackPopSingleList(
      StackClipStore s, final Consumer<T> callback) {
    final Integer count = s.stackTop();
    s = s.popStack();
    return stackPopSingleList(s, count, callback);
  }

  public static <T> StackClipStore stackPopSingleList(
      StackClipStore s, final int length, final Consumer<T> callback) {
    for (int i = 0; i < length; ++i) {
      callback.accept(s.stackTop());
      s = s.popStack();
    }
    return s;
  }
}
