package com.zarbosoft.pidgoon.bytes.stores;

import com.zarbosoft.pidgoon.Store;
import com.zarbosoft.pidgoon.bytes.Clip;
import com.zarbosoft.pidgoon.bytes.Position;
import com.zarbosoft.pidgoon.internal.BranchingStack;
import org.pcollections.PVector;
import org.pcollections.TreePVector;

import java.util.Map;

public abstract class ClipStore extends Store {

  protected BranchingStack<Clip> data;

  public ClipStore() {
    this(null);
  }

  public ClipStore(Map env) {
    super(null, TreePVector.empty(), env);
    data = new BranchingStack<>(new Clip());
  }

  /**
   * Constructor for splitting
   *
   * @param data
   * @param color
   */
  protected ClipStore(
      final Object color,
      final PVector<Object> cutStops,
      Map env,
      final BranchingStack<Clip> data) {
    super(color, cutStops, env);
    this.data = data;
  }

  public Clip topData() {
    return data.top();
  }

  @Override
  public Store pop() {
    return pop(true);
  }

  public Store pop(final boolean combine) {
    final Clip top = data.top();
    final BranchingStack<Clip> above = data.pop();
    final ClipStore out = split();
    if (combine) out.data = above.set(above.top().cat(top));
    else out.data = above;
    return out;
  }

  @Override
  public Store push() {
    final ClipStore out = split();
    out.data = data.push(new Clip());
    return out;
  }

  @Override
  public Store inject(final long size) {
    final Clip top = data.top();
    BranchingStack<Clip> pointer = data.pop();
    for (long i = 0; i < size; ++i) pointer = pointer.push(new Clip());
    pointer = pointer.push(top);
    final ClipStore out = split();
    out.data = pointer;
    return out;
  }

  @Override
  public Store record(final com.zarbosoft.pidgoon.Position position) {
    final ClipStore out = split();
    out.data = data.set(data.top().cat(((Position) position).getStoreData()));
    return out;
  }

  public Store setData(final Clip c) {
    final ClipStore out = split();
    out.data = this.data.set(c);
    return out;
  }
}
