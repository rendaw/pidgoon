package com.zarbosoft.pidgoon.bytes.nodes;

import com.google.common.collect.BoundType;
import com.google.common.collect.ImmutableRangeSet;
import com.google.common.collect.Range;
import com.google.common.collect.RangeSet;
import com.zarbosoft.pidgoon.Node;
import com.zarbosoft.pidgoon.bytes.Position;
import com.zarbosoft.pidgoon.errors.InvalidGrammar;
import com.zarbosoft.pidgoon.internal.Parent;
import com.zarbosoft.pidgoon.State;
import com.zarbosoft.pidgoon.Store;
import com.zarbosoft.pidgoon.nodes.Reference.RefParent;
import com.zarbosoft.pidgoon.parse.Parse;
import org.pcollections.PMap;

import java.util.List;

/**
 * A terminal is a node that matches a single byte. Compares the read bye to a set of specified byte
 * ranges.
 */
public class Terminal extends Node {
  private final RangeSet<Byte> value;

  public Terminal(final RangeSet<Byte> value) {
    this.value = value;
  }

  @SafeVarargs
  public Terminal(final Byte... units) {
    if (units.length == 0) throw new InvalidGrammar("Empty terminal specification!");
    final ImmutableRangeSet.Builder<Byte> builder = ImmutableRangeSet.builder();
    for (final Byte unit : units) {
      builder.add(Range.closedOpen(unit, (byte) (unit + 1)));
    }
    value = builder.build();
  }

  public Terminal(Range<Byte> range) {
    range =
        Range.closedOpen(
            (byte)
                (range.lowerBoundType() == BoundType.CLOSED
                    ? range.lowerEndpoint()
                    : range.lowerEndpoint() - 1),
            (byte)
                (range.upperBoundType() == BoundType.OPEN
                    ? range.upperEndpoint()
                    : range.upperEndpoint() + 1));
    final ImmutableRangeSet.Builder<Byte> builder = ImmutableRangeSet.builder();
    builder.add(range);
    value = builder.build();
  }

  public Terminal(final List<Byte> units) {
    final ImmutableRangeSet.Builder<Byte> builder = ImmutableRangeSet.builder();
    for (final Byte unit : units) {
      builder.add(Range.closedOpen(unit, (byte) (unit + 1)));
    }
    value = builder.build();
  }

  /**
   * Creates a terminal that matches any of the listed characters. Note that only one of the
   * characters matches. Use Grammar.stringSequence to match a sequence.
   *
   * @param units
   * @return
   */
  public static Terminal fromChar(final char... units) {
    final Byte[] converted = new Byte[units.length];
    for (int i = 0; i < units.length; ++i) converted[i] = (Byte) (byte) units[i];
    return new Terminal(converted);
  }

  @Override
  public void context(
      final Parse context,
      final Store prestore,
      final Parent parent,
      final PMap<Object, RefParent> seen,
      final Object cause) {
    context.leaves.add(
        new State(prestore) {
          @Override
          public <T> T color() {
            return (T) prestore.color;
          }

          @Override
          public void parse(
              final Parse step, final com.zarbosoft.pidgoon.Position sourcePosition) {
            Store store = prestore;
            final Position position = (Position) sourcePosition;
            store = store.record(position);
            if (value.contains(position.get())) {
              parent.advance(step, store, this);
            } else {
              parent.error(step, store, this);
            }
          }
        });
  }
}
