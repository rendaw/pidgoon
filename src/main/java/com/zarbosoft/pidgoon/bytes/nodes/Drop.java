package com.zarbosoft.pidgoon.bytes.nodes;

import com.zarbosoft.pidgoon.Node;
import com.zarbosoft.pidgoon.bytes.stores.StackClipStore;
import com.zarbosoft.pidgoon.internal.BaseParent;
import com.zarbosoft.pidgoon.internal.Parent;
import com.zarbosoft.pidgoon.Store;
import com.zarbosoft.pidgoon.nodes.Reference;
import com.zarbosoft.pidgoon.parse.Parse;
import org.pcollections.PMap;

/**
 * In byte parses causes the byte sequence matched by the trial to be erased from the matched data.
 */
public class Drop extends Node {
  public final Node child;

  public Drop(final Node child) {
    this.child = child;
  }

  @Override
  public void context(
      final Parse context,
      final Store store,
      final Parent parent,
      final PMap<Object, Reference.RefParent> seen,
      final Object cause) {
    child.context(
        context,
        store.push(),
        new BaseParent(parent) {
          @Override
          public void advance(final Parse step, final Store store, final Object cause) {
            parent.advance(step, ((StackClipStore) store).pop(false), cause);
          }
        },
        seen,
        cause);
  }
}
