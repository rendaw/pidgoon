package com.zarbosoft.pidgoon.bytes;

import com.google.common.primitives.Bytes;
import com.zarbosoft.pidgoon.bytes.nodes.Terminal;
import com.zarbosoft.pidgoon.bytes.stores.ClipStore;
import com.zarbosoft.pidgoon.bytes.stores.StackClipStore;
import com.zarbosoft.pidgoon.errors.InvalidStream;
import com.zarbosoft.pidgoon.internal.BaseParseBuilder;
import com.zarbosoft.pidgoon.nodes.Sequence;
import com.zarbosoft.pidgoon.parse.Parse;
import com.zarbosoft.rendaw.common.Pair;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;

public class ParseBuilder<O> extends BaseParseBuilder<ParseBuilder<O>> {
  private ParseBuilder(final ParseBuilder<O> other) {
    super(other);
  }

  public ParseBuilder() {}

  public static Sequence byteSeq(final List<Byte> list) {
    final Sequence out = new Sequence();
    list.stream().forEach(b -> out.add(new Terminal(b)));
    return out;
  }

  public static Sequence stringSeq(final String string) {
    final Sequence out = new Sequence();
    Bytes.asList(string.getBytes(StandardCharsets.UTF_8)).stream()
        .forEach(b -> out.add(new Terminal(b)));
    return out;
  }

  public O parse(final String string) {
    return parse(new ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8)));
  }

  public O parse(final InputStream stream) {
    Position position = new Position(stream);
    final ClipStore store = initialStore == null ? new StackClipStore(env == null ? null : new HashMap<>(env)) : (ClipStore) initialStore;
    Parse context =
        Parse.prepare(grammar, root, store, errorHistoryLimit, uncertaintyLimit, dumpAmbiguity);
    if (position.isEOF()) return null;
    while (!position.isEOF()) {
      context = context.step(position);
      position = position.advance();
    }
    if (context.results.isEmpty()) return null;
    return (O) context.results.get(0);
  }

  /**
   * Parse until the stream stops matching.
   *
   * @param stream
   * @return
   */
  public Pair<Parse, Position> longestMatchFromStart(final InputStream stream) {
    Position position = new Position(stream);
    final ClipStore store = initialStore == null ? new StackClipStore(env == null ? null : new HashMap<>(env)) : (ClipStore) initialStore;
    Parse context =
        Parse.prepare(grammar, root, store, errorHistoryLimit, uncertaintyLimit, dumpAmbiguity);
    Pair<Parse, Position> record = new Pair<>(context, position);
    if (position.isEOF()) return record;
    while (!position.isEOF()) {
      try {
        context = context.step(position);
      } catch (final InvalidStream e) {
        break;
      }
      position = position.advance();
      record = new Pair<>(context, position);
      if (context.leaves.isEmpty()) break;
    }
    return record;
  }

  @Override
  protected ParseBuilder<O> split() {
    return new ParseBuilder<>(this);
  }
}
