package com.zarbosoft.pidgoon;

import com.zarbosoft.pidgoon.parse.Parse;
import org.pcollections.PVector;

public abstract class State {
  public final PVector<Object> cutStops;

  protected State(final Store store) {
    this.cutStops = store.cutStops;
  }

  /**
   * The current color of this branch, as set by a Color node
   *
   * @param <T>
   * @return
   */
  public abstract <T> T color();

  public abstract void parse(Parse step, Position position);
}
