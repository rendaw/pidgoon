package com.zarbosoft.pidgoon.events.stores;

import com.zarbosoft.pidgoon.Position;
import com.zarbosoft.pidgoon.Store;
import com.zarbosoft.pidgoon.events.Event;
import com.zarbosoft.pidgoon.internal.BranchingStack;
import com.zarbosoft.pidgoon.nodes.Operator;
import com.zarbosoft.rendaw.common.Pair;
import org.pcollections.PVector;
import org.pcollections.TreePVector;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class StackStore extends Store {
  private Event top;
  private BranchingStack<Object> stack;

  public StackStore() {
    this(null);
  }

  public StackStore(Map env) {
    super(null, TreePVector.empty(), env);
    top = null;
    stack = null;
  }

  @Override
  public <Y> Y split() {
    return (Y) new StackStore(color, cutStops, env, stack, top);
  }

  public StackStore(
      final Object color,
      final PVector<Object> cutStops,
      final Map env,
      final BranchingStack<Object> stack,
      final Event top) {
    super(color, cutStops, env);
    this.top = top;
    this.stack = stack;
    this.color = color;
  }

  /**
   * count element must already be on stack
   *
   * @return
   */
  public StackStore stackSingleElement() {
    StackStore store = this;
    Object value = store.stackTop();
    store = store.popStack();
    return store.stackSingleElement(value);
  }

  public StackStore stackSingleElement(Object value) {
    StackStore store = this;
    final int length = store.stackTop();
    store = store.popStack();
    return store.pushStack(value).pushStack(length + 1);
  }

  public StackStore popStack() {
    final StackStore out = split();
    out.stack = stack.pop();
    return out;
  }

  @Override
  public boolean hasResult() {
    return (stack != null) && stack.isLast();
  }

  public StackStore pushStack(final Object o) {
    final StackStore out = new StackStore();
    if (stack == null) {
      out.stack = new BranchingStack<>(o);
    } else {
      out.stack = stack.push(o);
    }
    return out;
  }

  @Override
  public Object result() {
    return stackTop();
  }

  public static Operator<StackStore> prepVarStack = new Operator<StackStore>() {
    @Override
    protected StackStore process(StackStore store) {
      return store.pushStack(0);
    }
  };

  public static Operator<StackStore> pushVarStackSingle = new Operator<StackStore>() {
    @Override
    protected StackStore process(StackStore store) {
      return store.stackSingleElement();
    }
  };

  public StackStore stackVarDoubleElement() {
    StackStore store = this;
    final Object right = store.stackTop();
    store = store.popStack();
    final Object left = store.stackTop();
    store = store.popStack();
    final int length = store.stackTop();
    store = store.popStack();
    return store.pushStack(new Pair<>(left, right)).pushStack(length + 1);
  }

  @Override
  public Store record(final Position position) {
    final StackStore out = split();
    out.top = ((com.zarbosoft.pidgoon.events.Position) position).get();
    return out;
  }

  public StackStore stackVarDoubleElement(final String name) {
    StackStore store = this;
    final Object value = store.stackTop();
    store = store.popStack();
    final int length = store.stackTop();
    store = store.popStack();
    return store.pushStack(new Pair<>(name, value)).pushStack(length + 1);
  }

  public StackStore stackFixedDoubleElement(final String name) {
    StackStore store = this;
    final Object value = store.stackTop();
    store = store.popStack();
    return store.pushStack(new Pair<>(name, value));
  }

  /**
   * The last event matched.
   *
   * @param <T>
   * @return
   */
  public <T> T stackTop() {
    return (T) stack.top();
  }

  public <L, R> StackStore popVarDoubleList(final Pair.Consumer<L, R> callback) {
    StackStore s = this;
    final Integer count = s.stackTop();
    s = s.popStack();
    return s.popFixedDoubleList(count, callback);
  }

  public <L, R> StackStore popFixedMap(final int length, Map<L, R> dest) {
    return popFixedDoubleList(length, (L l, R r) -> dest.put(l, r));
  }

  public <L, R> StackStore popVarMap(Map<L, R> dest) {
    return popVarDoubleList((L l, R r) -> dest.put(l, r));
  }

  public <L, R> StackStore popFixedDoubleList(
      final int length, final Pair.Consumer<L, R> callback) {
    StackStore s = this;
    for (int i = 0; i < length; ++i) {
      final Object l = s.stackTop();
      s = s.popStack();
      final Object r = s.stackTop();
      s = s.popStack();
      callback.accept((L) l, (R) r);
    }
    return s;
  }

  public <T> StackStore popFixedSingle(final int length, Consumer<T> consumer) {
    StackStore s = this;
    for (int i = 0; i < length; ++i) {
      consumer.accept(s.stackTop());
      s = s.popStack();
    }
    return s;
  }

  public <T> StackStore popVarSingle(Consumer<T> consumer) {
    StackStore s = this;
    final Integer count = s.stackTop();
    s = s.popStack();
    return s.popFixedSingle(count, consumer);
  }

  public <T> StackStore popVarSingleList(List<T> out) {
    StackStore s = this;
    final Integer count = s.stackTop();
    s = s.popStack();
    return s.popFixedSingleList(count, out);
  }

  public <T> StackStore popFixedSingleList(final int length, List<T> out) {
    StackStore s = this;
    for (int i = 0; i < length; ++i) {
      out.add(s.stackTop());
      s = s.popStack();
    }
    return s;
  }

  public Event top() {
    return top;
  }
}
