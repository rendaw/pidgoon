package com.zarbosoft.pidgoon.events;

import com.zarbosoft.pidgoon.Store;
import com.zarbosoft.pidgoon.events.stores.StackStore;
import com.zarbosoft.pidgoon.internal.BaseParseBuilder;
import com.zarbosoft.rendaw.common.Common;
import com.zarbosoft.rendaw.common.Pair;

import java.util.HashMap;
import java.util.stream.Stream;

public class ParseBuilder<O> extends BaseParseBuilder<ParseBuilder<O>> {
  protected ParseBuilder(final ParseBuilder<O> other) {
    super(other);
  }

  public ParseBuilder() {}

  /**
   * Parse by pulling events from the stream.
   *
   * @param data
   * @return
   */
  public O parse(final Stream<Pair<? extends Event, Object>> data) {
    final Common.Mutable<ParseEventSink<O>> eventStream = new Common.Mutable<>(parse());
    data.forEach(
        pair -> {
          eventStream.value = eventStream.value.push(pair.first, pair.second.toString());
        });
    return eventStream.value.result();
  }

  /**
   * Instead of pulling from an input stream, use the returned EventStream to push events to the
   * parse.
   *
   * @return
   */
  public ParseEventSink<O> parse() {
    final Store store =
        initialStore == null
            ? new StackStore(env == null ? null : new HashMap<>(env))
            : initialStore;
    return new ParseEventSink<>(
        grammar, root, store, errorHistoryLimit, uncertaintyLimit, dumpAmbiguity);
  }

  @Override
  protected ParseBuilder<O> split() {
    return new ParseBuilder<>(this);
  }
}
