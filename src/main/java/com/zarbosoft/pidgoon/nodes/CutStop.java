package com.zarbosoft.pidgoon.nodes;

import com.zarbosoft.pidgoon.Node;
import com.zarbosoft.pidgoon.internal.BaseParent;
import com.zarbosoft.pidgoon.internal.Parent;
import com.zarbosoft.pidgoon.Store;
import com.zarbosoft.pidgoon.parse.Parse;
import org.pcollections.PMap;

/**
 * When a cut matches leaves created within that parse tree's closest CutStop with a matching name
 * are removed. If the cut has no name, the closest CutStop is used, regardless of its name.
 */
public class CutStop extends Node {
  private final Node child;
  private String name;

  public CutStop(final Node child) {
    this.child = child;
    name = null;
  }

  public CutStop name(final String name) {
    this.name = name;
    return this;
  }

  @Override
  public void context(
      final Parse context,
      final Store prestore,
      final Parent parent,
      final PMap<Object, Reference.RefParent> seen,
      final Object cause) {
    final Parent newParent =
        new BaseParent(parent) {
          @Override
          public void cut(final Parse step, final String name) {
            if (name == null || name.equals(CutStop.this.name)) {
              step.cut(this);
            } else super.cut(step, name);
          }

          @Override
          public void advance(final Parse step, final Store prestore, final Object cause) {
            final Store store = prestore.split();
            store.cutStops = store.cutStops.minus(store.cutStops.size() - 1);
            parent.advance(step, store, cause);
          }
        };
    final Store store = prestore.split();
    store.cutStops = store.cutStops.plus(newParent);
    child.context(context, store, newParent, seen, cause);
  }
}
