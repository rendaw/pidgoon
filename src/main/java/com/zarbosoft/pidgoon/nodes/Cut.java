package com.zarbosoft.pidgoon.nodes;

import com.zarbosoft.pidgoon.Node;
import com.zarbosoft.pidgoon.internal.BaseParent;
import com.zarbosoft.pidgoon.internal.Parent;
import com.zarbosoft.pidgoon.Store;
import com.zarbosoft.pidgoon.parse.Parse;
import org.pcollections.PMap;

/**
 * Drop all current branches to the nearest clip store or root this when the child node matches.
 * This saves memory when you're sure the child is the correct parse.
 */
public class Cut extends Node {
  private final Node child;
  private String name;

  public Cut(final Node child) {
    this.child = child;
    this.name = null;
  }

  public Cut name(final String name) {
    this.name = name;
    return this;
  }

  @Override
  public void context(
      final Parse context,
      final Store store,
      final Parent parent,
      final PMap<Object, Reference.RefParent> seen,
      final Object cause) {
    child.context(
        context,
        store,
        new BaseParent(parent) {
          @Override
          public void advance(final Parse step, final Store store, final Object cause) {
            parent.cut(step, name);
            parent.advance(step, store, cause);
          }
        },
        seen,
        cause);
  }
}
